#Aleksandra Kuriata
include("./methods.jl")
using methods

function task5()
  epsilon = 1e-4
  delta = 1e-4
  a = -2e0
  b = 1e0
  c = 2e0
  f(x) = e^x
  g(x) = 3*x
  println("1 function call")
  println("-------------------------------------------------------------------------------")
  mbisekcji(x -> f(x) - g(x), a, b, delta, epsilon)

  println()

  println("2 function call")
  println("-------------------------------------------------------------------------------")
  mbisekcji(x -> f(x) - g(x), b, c, delta, epsilon)
end

task5()
