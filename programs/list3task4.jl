#Aleksandra Kuriata
include("./methods.jl")
using methods

function task4()
  f(x) =  sin(x) - (x/2)^2
  pf(x) = cos(x) - x/2
  delta = 5e-6
  epsilon = 5e-6

  a = 1.5
  b = 2.0

  x0mstycznych = 1.5

  x0msiecznych = 1.0
  x1 = 2.0

  maxit = 20
  println("Result for bisection method: ")
  println("---------------------------------------------------------------------------------")
  bisectionMethod = mbisekcji(x -> f(x), a, b, delta, epsilon)
  println("")
  println("Result for newton method: ")
  println("---------------------------------------------------------------------------------")
  newtonMethod = mstycznych(x -> f(x), x -> pf(x), x0mstycznych, delta, epsilon, maxit)
  println("")
  println("Result for bisection method: ")
  println("---------------------------------------------------------------------------------")
  secantMethod = msiecznych(x -> f(x), x0msiecznych, x1, delta, epsilon, maxit)
  println("")

  println("")
  println("")
  println("Results for: ")
  println("-------------------------------------------")
  println("bisection method: $(bisectionMethod)")
  println("Newton method: $(newtonMethod)")
  println("secant method: $(secantMethod)")
end

task4()
