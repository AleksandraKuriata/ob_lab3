#Aleksandra Kuriata
include("./methods.jl")
using methods

function bisection(f1, f2, a, b, delta, epsilon)
  println("f1(x) = e^(1-x)-1: ")
  mbisekcji(f1, a, b, delta, epsilon)
  println("")
  println("f2(x) =  x*e^(-x): ")
  mbisekcji(f2, a, b, delta, epsilon)
end

function newtons(f1, f2, pf1, pf2, x0, delta, epsilon, maxiter)
  println("f1(x) = e^(1-x)-1: ")
  mstycznych(f1, pf1, x0, delta, epsilon, maxiter)
  println("")
  println("f2(x) =  x*e^(-x): ")
  mstycznych(f2, pf2, x0, delta, epsilon, maxiter)
end

function secant(f1, f2, x0, x1, delta, epsilon, maxiter)
  println("f1(x) = e^(1-x)-1: ")
  msiecznych(f1, x0, x1, delta, epsilon, maxiter)
  println("")
  println("f2(x) =  x*e^(-x): ")
  msiecznych(f2, x0, x1, delta, epsilon, maxiter)
end

function task6()
  delta = 1e-5
  epsilon = 1e-5

  f1(x) = e^(1-x)-1
  pf1(x) = -e^(1-x)

  f2(x) =  x*e^(-x)
  pf2(x) = -e^(-x)*(x-1)

  maxiter = 100


  println("MBISEKCJI:")
  println("--------------------------------------------------------------------------------------------------------------")
  #mbisekcji
  a = -2e0
  b = 3e0
  bisection(x->f1(x), x->f2(x), a, b, delta, epsilon)
  println("")

  println("MSTYCZNYCH:")
  println("--------------------------------------------------------------------------------------------------------------")
  #mstycznych
  x0 = 5e-1
  newtons(x->f1(x), x->f2(x), x->pf1(x), x->pf2(x), x0, delta, epsilon, maxiter)
  println("")

  println("MSIECZNYCH:")
  println("--------------------------------------------------------------------------------------------------------------")
  #msiecznych
  x0 = -1e0
  x1 = 0.5e0
  secant(x->f1(x),x->f2(x), x0, x1, delta, epsilon, maxiter)
  println("")
  println("")
  println("")


  println("TEST1 for f1 xo belongs to (1 do inf]:")
  println("-------------------------------------------------------")
  #mstycznych tests
  #1
  x0f1 = 100e0
  mstycznych(f1, pf1, x0f1, delta, epsilon, maxiter)
  println("")

  println("TEST2 for f2 xo>1:")
  println("-------------------------------------------------------")
  #2
  x0f2 = 100e0
  mstycznych(f2, pf2, x0f2, delta, epsilon, maxiter)
  println("")

  println("TEST3 for f2 xo = 1:")
  println("-------------------------------------------------------")
  #3
  x0f2 = 1e0
  mstycznych(f2, pf2, x0f2, delta, epsilon, maxiter)
  println("")

end

task6()
