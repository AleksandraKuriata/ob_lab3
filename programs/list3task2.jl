#Aleksandra Kuriata
include("./methods.jl")
using methods

function task2()
  f(x) = x^3-4
  pf(x) = 3*x^2
  x0 = 3e0
  delta = 5e-10
  epsilon = 5e-10
  maxit = 1000

  mstycznych(x -> f(x), x -> pf(x), x0, delta, epsilon, maxit)
end

 task2()
