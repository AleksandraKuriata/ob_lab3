#Aleksandra Kuriata
module methods
    export mbisekcji
    export mstycznych
    export msiecznych

    function mbisekcji(f, a::Float64, b::Float64, delta::Float64, epsilon::Float64)
      f1 = f(a)
      f2 = f(b)
      err = 0

      if sign(f1) == sign(f2)
        err = 1
        println("Function doesnt change the character in the interval.")
        return err
      end

      range = b - a

      r = a + range/2
      v = f(r)

      it = 0
      while abs(range) > delta && abs(v) > epsilon
        r = a + range/2
        v = f(r)

        if sign(v) != sign(f1)
          b = r
          f2 = v

        else
          a = r
          f1 = v
        end

        it = it + 1
        range = range/2

      end
      println("approximation of the root of the equation f(x) = 0: $(r)")
      println("value f(r): $(v)")
      println("number of iterations executed: $(it)")
      println("error signaling: $(err)")

      return r, v, it, err
    end

    function mstycznych(f, pf, x0::Float64, delta::Float64, epsilon::Float64, maxit::Int)
      v = f(x0)
      it = 0
      err = 0

      if abs(v) < epsilon
        r = x0
        println("approximation of the root of the equation f(r) = 0: $(r)")
        println("value f(r): $(v)")
        println("number of iterations executed: $(it)")
        println("error signaling: convergent method: $(err)")
        return r, v, it, err
      end

      for it = 1:maxit
        derivative = pf(x0)

        if derivative < 1e-20
          err = 2
        end

        r = x0 - v/derivative
        v = f(r)

        if abs(r - x0) < delta || abs(v) <epsilon
          println("approximation of the root of the equation f(x) = 0: $(r)")
          println("value f(r): $(v)")
          println("number of iterations executed: $(it)")
          println("error signaling: convergent method: $(err)")
          return r, v, it, err
        end

        x0 = r
      end

      err = 1
      println("approximation of the root of the equation f(x) = 0: $(r)")
      println("value f(r): $(v)")
      println("number of iterations executed: $(it)")
      println("error signaling: the required accuracy has not been reached in maxit iterationb: $(err)")
      return r, v, it, err
    end

    function swap(x0::Float64, x1::Float64)
      return x1, x0
    end

    function msiecznych(f, x0::Float64, x1::Float64, delta::Float64, epsilon::Float64, maxit::Int)
      err = 0
      r = x0
      v = f(r)
      fx1 = f(x1)
      for it = 1:maxit
        if abs(v) > abs(fx1)
          r, x1 = swap(r, x1)
          v, fx1 = swap(v, fx1)
        end

        x = (x1 - r)/(fx1 - v)
        x1 = r
        fx1 = v
        r = r - v * x
        v = f(r)

        if abs(x1 - r) < delta || abs(v) < epsilon
          println("approximation of the root of the equation f(x) = 0: $(r)")
          println("value f(r): $(v)")
          println("number of iterations executed: $(it)")
          println("error signaling: $(err)")
          return r, v ,it ,err
        end
      end
      err = 1
      println("approximation of the root of the equation f(x) = 0: $(r)")
      println("value f(r): $(v)")
      println("number of iterations executed: $(it)")
      println("error signaling: $(err)")
      return r, v ,it ,err
    end

end
