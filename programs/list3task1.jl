#Aleksandra Kuriata
include("./methods.jl")
using methods

function task1()
  f(x) = x^3-4

  a = -1e0
  b =  5e0

  delta = 5e-10
  epsilon = 5e-10

  mbisekcji(x -> f(x), a, b, delta, epsilon)
end

task1()
